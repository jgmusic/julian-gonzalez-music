(() => {
    const refs = {
      openMenuBtn: document.querySelector(".menu-open-btn"),
      closeMenuBtn: document.querySelector(".menu-close-btn"),
      menuLink: document.querySelectorAll('.mob-link'),
      menu: document.querySelector(".mob-menu"),
    };
  
    refs.openMenuBtn.addEventListener("click", toggleMenu);
    refs.closeMenuBtn.addEventListener("click", toggleMenu);
    refs.menuLink.forEach(el => el.addEventListener("click", toggleMenu))
  
    function toggleMenu() {
      refs.menu.classList.toggle("is-hidden");
    }
  })();
